async function fetchTrainingPrograms(ordering) {
    let response = await sendRequest("GET", `${HOST}/api/trainingprograms/?ordering=${ordering}`);

    if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
    } else {
        let data = await response.json();

        let trainingprograms = data.results;
        let container = document.getElementById('div-content');
        trainingprograms.forEach(trainingprogram => {
            let templateTrainingProgram = document.querySelector("#template-trainingprogram");
            let cloneTrainingProgram = templateTrainingProgram.content.cloneNode(true);

            let aTrainingProgram = cloneTrainingProgram.querySelector("a");
            aTrainingProgram.href = `trainingprogram.html?id=${trainingprogram.id}`;

            let h5 = aTrainingProgram.querySelector("h5");
            h5.textContent = trainingprogram.name;

            let startDate = new Date(trainingprogram.start_date).toLocaleDateString();
            let endDate = new Date(trainingprogram.end_date).toLocaleDateString();

            let table = aTrainingProgram.querySelector("table");
            let rows = table.querySelectorAll("tr");
            rows[0].querySelectorAll("td")[1].textContent = startDate; // Date
            rows[1].querySelectorAll("td")[1].textContent = endDate; // Time
            rows[2].querySelectorAll("td")[1].textContent = trainingprogram.owner; //Owner

            container.appendChild(aTrainingProgram);
        });
        return trainingprograms;
    }
}

function createTrainingProgram() {
    window.location.replace("trainingprogram.html");
}

window.addEventListener("DOMContentLoaded", async () => {
    let createButton = document.querySelector("#btn-create-trainingprogram");
    createButton.addEventListener("click", createTrainingProgram);
    let ordering = "-date";

    const urlParams = new URLSearchParams(window.location.search);
    if (urlParams.has('ordering')) {
        ordering = urlParams.get('ordering');
        if (ordering == "name" || ordering == "owner" || ordering == "date") {
                let aSort = document.querySelector(`a[href="?ordering=${ordering}"`);
                aSort.href = `?ordering=-${ordering}`;
        } 
    } 

    let currentSort = document.querySelector("#current-sort");
    currentSort.innerHTML = (ordering.startsWith("-") ? "Descending" : "Ascending") + " " + ordering.replace("-", "");

    let currentUser = await getCurrentUser();
    // grab username
    if (ordering.includes("owner")) {
        ordering += "__username";
    }
    let trainingprograms = await fetchTrainingPrograms(ordering);
    
    let tabEls = document.querySelectorAll('a[data-bs-toggle="list"]');
    for (let i = 0; i < tabEls.length; i++) {
        let tabEl = tabEls[i];
        tabEl.addEventListener('show.bs.tab', function (event) {
            let trainingprogramAnchors = document.querySelectorAll('.trainingprogram');
            for (let j = 0; j < trainingprograms.length; j++) {
                let trainingprogram = trainingprograms[j];
                let trainingprogramAnchor = trainingprogramAnchors[j];

                switch (event.currentTarget.id) {
                    case "list-my-trainingprograms-list":
                        if (trainingprogram.owner == currentUser.url) {
                            trainingprogramAnchor.classList.remove('hide');
                        } else {
                            trainingprogramAnchor.classList.add('hide');
                        }
                        break;
                    case "list-athlete-trainingprograms-list":
                        if (currentUser.athletes && currentUser.athletes.includes(trainingprogram.owner)) {
                            trainingprogramAnchor.classList.remove('hide');
                        } else {
                            trainingprogramAnchor.classList.add('hide');
                        }
                        break;
                    default :
                        trainingprogramAnchor.classList.remove('hide');
                        break;
                }
            }
        });
    }
});
