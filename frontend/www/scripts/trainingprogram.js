let cancelTrainingProgramButton;
let okTrainingProgramButton;
let deleteTrainingProgramButton;
let editTrainingProgramButton;
let areValidDates; 
let areDatesEmpty; 

async function retrieveTrainingProgram(id) {  
    let trainingprogramData = null;
    let response = await sendRequest("GET", `${HOST}/api/trainingprograms/${id}/`);
    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert("Could not retrieve training program data!", data);
        document.body.prepend(alert);
    } else {
        trainingprogramData = await response.json();
        let form = document.querySelector("#form-trainingprogram");
        let formData = new FormData(form);

        for (let key of formData.keys()) {
            let selector = `input[name="${key}"], textarea[name="${key}"]`;
            let input = form.querySelector(selector);
            let newVal = trainingprogramData[key];
            if (key == "start_date") {
                // Creating a valid datetime-local string with the correct local time
                let date = new Date(newVal);
                date = new Date(date.getTime() - (date.getTimezoneOffset() * 60 * 1000)).toISOString(); // get ISO format for local time
                newVal = date.substring(0, newVal.length - 1);    // remove Z (since this is a local time, not UTC)
            }
            if (key == "end_date") {
                let date = new Date(newVal);
                date = new Date(date.getTime() - (date.getTimezoneOffset() * 60 * 1000)).toISOString(); // get ISO format for local time
                newVal = date.substring(0, newVal.length - 1);  
            }
            input.value = newVal;

        }
        setDaysForTrainingProgram(trainingprogramData);
        let input = form.querySelector("select:disabled");
        input.value = trainingprogramData["visibility"];
    }
    return trainingprogramData;     
}

function setDaysForTrainingProgram(trainingprogramData) {
    const elements = document.querySelectorAll('.weekday');
    Array.from(elements).forEach((element, index) => {
        let res = trainingprogramData["days"].match(element.value);
        if(res != null) {
            element.checked = true;
        } else {
            return
        }
    });
}

function handleCancelDuringTrainingProgramEdit() {
    location.reload();
}

function handleEditTrainingProgramButtonClick() { 
    setReadOnly(false, "#form-trainingprogram");
    document.querySelector("#inputOwner").readOnly = true;  // owner field should still be readonly 

    editTrainingProgramButton.className += " hide";
    okTrainingProgramButton.className = okTrainingProgramButton.className.replace(" hide", "");
    cancelTrainingProgramButton.className = cancelTrainingProgramButton.className.replace(" hide", "");
    deleteTrainingProgramButton.className = deleteTrainingProgramButton.className.replace(" hide", "");
    cancelTrainingProgramButton.addEventListener("click", handleCancelDuringTrainingProgramEdit);

}

async function deleteTrainingProgram(id) {
    let response = await sendRequest("DELETE", `${HOST}/api/trainingprograms/${id}/`);
    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert(`Could not delete training program ${id}!`, data);
        document.body.prepend(alert);
    } else {
        window.location.replace("trainingprograms.html");
    }
}

async function updateTrainingProgram(id) {
    let submitForm = generateTrainingProgramForm();

    let response = await sendRequest("PUT", `${HOST}/api/trainingprograms/${id}/`, submitForm, "");
    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert("Could not update training program!", data);
        document.body.prepend(alert);
    } else {
        location.reload();
    }
}

function generateTrainingProgramForm() {
    let form = document.querySelector("#form-trainingprogram");
    let formData = new FormData(form);
    let submitForm = new FormData();

    submitForm.append("name", formData.get('name'));
    try {
        let start_date = new Date(formData.get('start_date')).toISOString();
        let end_date = new Date(formData.get('end_date')).toISOString();
        submitForm.append("start_date", start_date);
        submitForm.append("end_date", end_date);
        submitForm.append("notes", formData.get("notes"));
        submitForm.append("visibility", formData.get("visibility"));
        let days = getSelectedDays()
        submitForm.append("days", days);
        if (start_date >= end_date) {
            areValidDates = false;     
        }
    }
    catch (e) {
        areDatesEmpty = true; 
    }
    return submitForm;
}

function getSelectedDays() {
    let days = [];
    const elements = document.querySelectorAll('.weekday');
    Array.from(elements).forEach((element, index) => {
        if(element.checked) {
            days.push(element.value);
        }
    });
    if(days.length == 0) {
        return "NONE"
    }
    daysString = days.join('')
    return daysString;
}

async function createTrainingProgram() {
    let submitForm = generateTrainingProgramForm();
    let currentUser = await getCurrentUser();
    submitForm.append("email", currentUser.email);
    if (areValidDates==false) {
        let alert = createSimpleAlert("Start date cannot be after end date!");
        document.body.prepend(alert);
        areValidDates = true;        
    }
    else if (areDatesEmpty) {
        let alert = createSimpleAlert("Must choose start date, end date, title and description!");
        document.body.prepend(alert);
        areDatesEmpty = false;   
    }
    else {
        let response = await sendRequest("POST", `${HOST}/api/trainingprograms/`, submitForm, "");

        if (response.ok) {
            window.location.replace("trainingprograms.html");
            let alert = createSimpleAlert("New training program has successfully been created!");
            document.body.prepend(alert);
        } else {
            let data = await response.json();
            let alert = createAlert("Could not create new training program!", data);
            document.body.prepend(alert);
        }
    }   
}

function handleCancelDuringTrainingProgramCreate() {
    window.location.replace("trainingprograms.html");
}

window.addEventListener("DOMContentLoaded", async () => {
    cancelTrainingProgramButton = document.querySelector("#btn-cancel-trainingprogram");
    okTrainingProgramButton = document.querySelector("#btn-ok-trainingprogram");
    deleteTrainingProgramButton = document.querySelector("#btn-delete-trainingprogram");
    editTrainingProgramButton = document.querySelector("#btn-edit-trainingprogram");

    const urlParams = new URLSearchParams(window.location.search);
    let currentUser = await getCurrentUser();

    if (urlParams.has('id')) {
        const id = urlParams.get('id');
        let trainingprogramData = await retrieveTrainingProgram(id);

        if (trainingprogramData["owner"] == currentUser.username) {
            editTrainingProgramButton.classList.remove("hide");
            editTrainingProgramButton.addEventListener("click", handleEditTrainingProgramButtonClick);
            deleteTrainingProgramButton.addEventListener("click", (async (id) => await deleteTrainingProgram(id)).bind(undefined, id));
            okTrainingProgramButton.addEventListener("click", (async (id) => await updateTrainingProgram(id)).bind(undefined, id));
        }
    } else {
        let ownerInput = document.querySelector("#inputOwner");
        ownerInput.value = currentUser.username;
        setReadOnly(false, "#form-trainingprogram");
        ownerInput.readOnly = !ownerInput.readOnly;

        okTrainingProgramButton.className = okTrainingProgramButton.className.replace(" hide", "");
        cancelTrainingProgramButton.className = cancelTrainingProgramButton.className.replace(" hide", "");

        okTrainingProgramButton.addEventListener("click", async () => await createTrainingProgram());
        cancelTrainingProgramButton.addEventListener("click", handleCancelDuringTrainingProgramCreate);
    }

});