from django.test import TestCase, Client  # Import is kept in case of future tests
from django.test import TestCase, RequestFactory
from django.contrib.sessions.middleware import SessionMiddleware
from users.models import User
from workouts.permissions import IsOwner, IsOwnerOfWorkout, IsCoachAndVisibleToCoach, IsCoachOfWorkoutAndVisibleToCoach, IsPublic, IsWorkoutPublic, IsReadOnly
from workouts.models import Workout, Exercise, ExerciseInstance
from comments.models import Comment, Like
import json
# Create your tests here.

class IsCommentPosted(TestCase):
    def setUp(self):
        self.client = Client()
        self.coach = User.objects.create(username="coach")
        self.coach.set_password('12345')
        self.coach.save()
    
    def test_UserCanPostComment(self):
        response = self.client.post('/api/token/', {'username': 'coach', 'password': '12345'})
        content = json.loads(response.content)
        self.client.defaults['HTTP_AUTHORIZATION'] = 'Bearer ' + content['access']
        
        workout = Workout.objects.create(name="Workout", date="2021-03-05T12:00:00Z", notes="Notes", owner=self.coach, visibility="PR")
        comment = Comment.objects.create(content="COMMENT", timestamp="2021-03-05T12:01:00Z", owner=self.coach, workout=workout)
        response = self.client.get('/api/comments/'+str(comment.id)+'/')
        self.assertEqual(response.status_code, 200)        
