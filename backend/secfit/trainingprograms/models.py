"""
Contains the models for the training programs Django application. Users
log training programs (Training Program).
"""
from django.db import models
from django.contrib.auth import get_user_model

class TrainingProgram(models.Model):
    """
    Django model for a training program that users can create.

    A training program has several attributes and describes information about what workouts the 
    user should do on specific days. It is however not linked to the workout models. This could
    be implemented in future work.

    Attributes:
        name:               Name of the training program
        start_date:         Dates the training program is planned to start
        end_date:           Dates the training program is planned to end
        notes:              Notes about the training program
        owner:              User that logged the training program
        visibility:         The visibility level of the training program: Coach, or Private
        days:               Which days the training program should be used on
    """

    name = models.CharField(max_length=100)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    notes = models.TextField()
    owner = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name="trainingprograms")

    # Visibility levels
    COACH = "CO"  # Visible only to owner and their coach
    PRIVATE = "PR"  # Visible only to owner
    VISIBILITY_CHOICES = [
        (COACH, "Coach"),
        (PRIVATE, "Private"),
    ]  # Choices for visibility level

    visibility = models.CharField(max_length=2, choices=VISIBILITY_CHOICES, default=COACH)
    days = models.CharField(max_length=20)

    class Meta:
        ordering = ["-start_date"]
