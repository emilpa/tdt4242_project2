"""
Contains custom DRF permissions classes for the training programs app
"""
from rest_framework import permissions

class IsOwner(permissions.BasePermission):
    """Checks whether the requesting user is also the owner of the existing object"""

    def has_object_permission(self, request, view, obj):
        return obj.owner == request.user

class IsAthleteOfCoach(permissions.BasePermission):
    """Checks whether the requesting user is the athlete of the coach which owns the training program"""
    
    def has_object_permission(self, request, view, obj):
        return request.user.coach == obj.trainingprogram.owner.coach
