"""
Tests for the training programs.
"""
from django.test import TestCase, Client
from users.models import User
from trainingprograms.models import TrainingProgram
import json

#System test
class TrainingProgramTest(TestCase):
        def setUp(self):
            self.client = Client()
            self.owner = User.objects.create(username="user")
            self.owner.set_password("password")
            self.owner.save()
            self.someuser = User.objects.create(username="someuser", coach=self.owner)
            self.someuser.set_password("password")
            self.someuser.save()
            response = self.client.post("/api/token/", {"username": "user", "password": "password"})
            self.client.defaults["HTTP_AUTHORIZATION"] = "Bearer " + json.loads(response.content)["access"]

        #See if training program can be created with all inputs good
        def test__trainingprogram_allgood(self):
            data = {
            'name': 'Training program',
            'start_date': '2021-03-23T18:56Z',
            'end_date': '2021-03-28T18:56Z',
            'notes': 'Noe her',
            'owner': self.owner,
            'visability': 'CO',
            'days': 'MO'
            }
            response = self.client.post('/api/trainingprograms/', data)
            self.assertEqual(response.status_code, 201)
       
       #See if training program can be created without name, should fail(400)
        def test__trainingprogram_no_name(self):
            data = {
            'name': '',
            'start_date': '2021-03-23T18:56Z',
            'end_date': '2021-03-28T18:56Z',
            'notes': 'Noe her',
            'owner': self.owner,
            'visability': 'CO',
            'days': 'MO'
            }
            response = self.client.post('/api/trainingprograms/', data)
            self.assertEqual(response.status_code, 400)
        
        #See if training program is created when end-date is before start-date, should succeed
        #because validation is frontend only
        def test__trainingprogram_end_date_before_start_date(self):
            data = {
            'name': 'Training Program',
            'start_date': '2021-03-23T18:56Z',
            'end_date': '2021-03-18T18:56Z',
            'notes': 'Noe her',
            'owner': self.owner,
            'visability': 'CO',
            'days': 'MO'
            }
            response = self.client.post('/api/trainingprograms/', data)
            self.assertEqual(response.status_code, 201)  
        
        #Test with no end-date
        def test__trainingprogram_no_end_date(self):
            data = {
            'name': 'Training Program',
            'start_date': '2021-03-23T18:56Z',
            'end_date': '',
            'notes': 'Noe her',
            'owner': self.owner,
            'visability': 'CO',
            'days': 'MO'
            }
            response = self.client.post('/api/trainingprograms/', data)
            self.assertEqual(response.status_code, 400)  
        
        #Test with no start-date
        def test__trainingprogram_no_start_date(self):
            data = {
            'name': 'Training Program',
            'start_date': '',
            'end_date': '2021-03-18T18:56Z',
            'notes': 'Noe her',
            'owner': self.owner,
            'visability': 'CO',
            'days': 'MO'
            }
            
            response = self.client.post('/api/trainingprograms/', data)
            self.assertEqual(response.status_code, 400) 
    
        #integration tests
        def test_coach_can_view_training_program(self):
            #Logging in as coach 
            response = self.client.post('/api/token/', {'username': 'user', 'password': 'password'})
            content = json.loads(response.content)
            self.client.defaults['HTTP_AUTHORIZATION'] = 'Bearer ' + content['access']                         
            
            #Coach can se athlete trainingprogram when set to coach visability
            trainingprogram = TrainingProgram.objects.create(name="Testprogram", start_date="2021-03-05T12:00:00Z",end_date="2021-03-14T12:00:00Z", notes="notes", owner=self.someuser, visibility="CO")
            response = self.client.get('/api/trainingprograms/'+str(trainingprogram.id)+'/')
            self.assertEqual(response.status_code, 200)
            
            #Coach can see public training program 
            trainingprogram = TrainingProgram.objects.create(name="Testprogram", start_date="2021-03-05T12:00:00Z",end_date="2021-03-16T12:00:00Z", notes="notes", owner=self.someuser, visibility="PU")
            response = self.client.get('/api/trainingprograms/'+str(trainingprogram.id)+'/')
            self.assertEqual(response.status_code, 200)
   