"""
Contains views for the training program application. These are mostly class-based views.
"""
from rest_framework import generics, mixins
from rest_framework import permissions

from rest_framework.parsers import JSONParser
from secfit.parsers import MultipartJsonParser
from rest_framework.response import Response
from django.db.models import Q
from rest_framework import filters
from trainingprograms.permissions import (IsOwner, IsAthleteOfCoach,)
from trainingprograms.models import TrainingProgram
from trainingprograms.serializers import TrainingProgramSerializer
from rest_framework.response import Response
from django.core.mail import send_mail

class SendTrainingProgramMail(mixins.CreateModelMixin, generics.GenericAPIView):
    """
    Class defining the web response for the sending an email

    HTTP methods: GET, POST
    """
    def post(self, request, *args, **kwargs):
        user = request.data.get("user")
        email = request.data.get("email")
        coach = request.data.get("sentfrom")
        name = request.data.get("trainingprogram")
        send_mail(
            '{} has created a new training program for you'.format(coach),
            'The name of the training program is {} and can be found in the list of your training programs'.format(
                name),
            'secfit2021@gmail.com',
            [email],
            fail_silently=False,
        )
        return Response(
            {
                "Email": email,
                "User": user
            }
        )

class TrainingProgramList(mixins.CreateModelMixin, mixins.DestroyModelMixin, generics.GenericAPIView):
    """
    Class defining the web response for the creation of a Training Program, or displaying a list
    of Training Programs

    HTTP methods: GET, POST
    """
    serializer_class = TrainingProgramSerializer
    permission_classes = [permissions.IsAuthenticated & (IsAthleteOfCoach | IsOwner)]  # User must be authenticated to create/view training programs
    parser_classes = [MultipartJsonParser, JSONParser]  # For parsing JSON 
    filter_backends = [filters.OrderingFilter]
    ordering_fields = ["name", "start_date", "end_date", "owner__username"]

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        email = request.data.get("email")
        name = request.data.get("name")
        # Send mail to user
        send_mail(
            'CONFIRMATION EMAIL: You have created a new training program',
            'Name of Training Program: {}'.format(name),
            'secfit2021@gmail.com',
            [email],
            fail_silently=False,
        )
        return self.create(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def get_queryset(self):
        qs = TrainingProgram.objects.none()
        if self.request.user:
            # A training program should be visible to the requesting user if any of the following hold:
            # - The owner of the training program is the requesting user
            # - The training program has coach visibility and the requesting user is the owner's coach
            qs = TrainingProgram.objects.filter(
                Q(owner=self.request.user)
                |
                (Q(visibility="CO") & Q(owner=self.request.user.coach))
            ).distinct()

        return qs

class TrainingProgramDetail(mixins.CreateModelMixin, mixins.DestroyModelMixin, mixins.RetrieveModelMixin, generics.GenericAPIView):
    """
    Class defining the web response for the details of an individual Training Program.

    HTTP methods: GET, PUT, DELETE
    """
    queryset = TrainingProgram.objects.all()
    serializer_class = TrainingProgramSerializer
    permission_classes = [permissions.IsAuthenticated]
    parser_classes = [MultipartJsonParser, JSONParser]

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)

