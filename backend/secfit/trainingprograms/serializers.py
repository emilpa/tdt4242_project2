"""
Serializers for the training program application
"""
from rest_framework import serializers
from trainingprograms.models import TrainingProgram

class TrainingProgramSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializer for an Training Program. Hyperlinks are used for relationships by default.

    Serialized fields: url, id, name, notes, owner, visibility, start_date, end_date, days

    """
    owner = serializers.ReadOnlyField(source="owner.username")

    class Meta:
        model = TrainingProgram
        fields = ["url", "id", "name", "notes", "owner",
                  "visibility", "start_date", "end_date", "days"]
        extra_kwargs = {"owner": {"read_only": True}}

    def create(self, validated_data):
        """
        Custom logic for creating a Training Program.

        Args:
            validated_data: Validated Training Program

        Returns:
            Training Program: A newly created Training Program
        """

        trainingprogram = TrainingProgram.objects.create(**validated_data)
        
        return trainingprogram

    def update(self, instance, validated_data):
        """
        Custom logic for updating a Training Program.

        Args:
            instance (Training Program): Current Training Program object
            validated_data: Contains data for validated fields

        Returns:
            Training Program: Updated Training Program instance
        """

        instance.name = validated_data.get("name", instance.name)
        instance.start_date = validated_data.get(
            "start_date", instance.start_date)
        instance.end_date = validated_data.get("end_date", instance.end_date)
        instance.notes = validated_data.get("notes", instance.notes)
        instance.visibility = validated_data.get(
            "visibility", instance.visibility)
        instance.days = validated_data.get("days", instance.days)
        instance.save()

        return instance

    def get_owner_username(self, obj):
        """
        Returns the owning user's username

        Args:
            obj (Training Program): Current Training Program

        Returns:
            str: Username of owner
        """
        return obj.owner.username
