from django.urls import path
from trainingprograms import views

urlpatterns = [
    path("api/trainingprograms/", views.TrainingProgramList.as_view(),
         name="trainingprogram-list"),
    path(
        "api/trainingprograms/<int:pk>/",
        views.TrainingProgramDetail.as_view(),
        name="trainingprogram-detail",
    ),
    path("api/send_mail/", views.SendTrainingProgramMail.as_view(), name="send_mail")
]
