"""
AppConfig for training programs app
"""
from django.apps import AppConfig

class TrainingProgramsConfig(AppConfig):
    """
    AppConfig for training programs app

    Attributes:
        name (str): The name of the application
    """

    name = "trainingprograms"
