"""
Module for registering models from training programs app to admin page so that they appear
"""
from django.contrib import admin
from .models import TrainingProgram

admin.site.register(TrainingProgram)
