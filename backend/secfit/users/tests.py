from django.test import TestCase, RequestFactory, Client
from workouts.models import Workout, Exercise, ExerciseInstance, WorkoutFile
from users.models import User
from comments.models import Comment
from trainingprograms.models import TrainingProgram
from users.serializers import UserSerializer

import json
# Create your tests here.
class CreateUserTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        #self.user = User.objects.create(username="testuser")
   
    def test_validate_password(self):
        user_password = {"password": "Password", "password1": "Password"}
        
        serializer = UserSerializer(user_password)
        
        password_validation = serializer.validate_password("password") 
        
        self.assertEqual(password_validation, "password")
        
    def test_create(self):
        user_data = {"username": "testuser", 
                     "email": "testusermail@test.com", 
                     "password": "Password", 
                     "phone_number" : "11122333", 
                     "country": "Ireland", 
                     "city": "Dublin", 
                     "street_address": "Street 4"}
        serialiser = UserSerializer(user_data)
        user = serialiser.create(user_data)
        self.assertEqual(user.username, user_data['username'])
        self.assertEqual(user.email, user_data['email'])
        self.assertEqual(user.phone_number, user_data['phone_number'])
        self.assertEqual(user.country, user_data['country'])
        self.assertEqual(user.city, user_data['city'])
        self.assertEqual(user.street_address, user_data['street_address'])

## BOUNDARY TESTS FOR REGISTRATION ##
class RegistrationTest(TestCase):
  def setUp(self):
    self.client = Client()
  
  def test__registration_allgood(self):
    data = {
      'email': 'madskills@mad.mad',
      'username': 'IamAthlete2021',
      'password': '#¤554!"#MO0d',
      'password1': '#¤554!"#MO0d',
      'phone_number': 123456789,
      'country': 'Somewhere',
      'city': 'Over',
      'street_address': 'The rainbow',
    }

    response = self.client.post('/api/users/', data)
    self.assertEqual(response.status_code, 201)
  def test_registration_without_username(self):
    data = {
      'email': 'madskills@mad.mad',
      'username': '',
      'password': '#¤554!"#MO0d',
      'password1': '#¤554!"#MO0d',
      'phone_number': 123456789,
      'country': 'Somewhere',
      'city': 'Over',
      'street_address': 'The rainbow',
    }

    response = self.client.post('/api/users/', data)
    self.assertEqual(response.status_code, 400)
  
  def test_registration_mandatory_info(self):
    data = {
      'email': '',
      'username': 'IamAthlete2021',
      'password': '#¤554!"#MO0d',
      'password1': '#¤554!"#MO0d',
      'phone_number': '',
      'country': '',
      'city': '',
      'street_address': '',
    }

    response = self.client.post('/api/users/', data)
    self.assertEqual(response.status_code, 201)

  def test_registration_without_password(self):
    data = {
     'email': 'madskills@mad.mad',
      'username': 'IamAthlete2021',
      'password': '',
      'password1': '',
      'phone_number': 123456789,
      'country': 'Somewhere',
      'city': 'Over',
      'street_address': 'The rainbow',
    }

    response = self.client.post('/api/users/', data)
    self.assertEqual(response.status_code, 400)


  


  def test_registration_without_password1(self):
    data = {
     'email': 'madskills@mad.mad',
      'username': 'IamAthlete2021',
      'password': '#¤554!"#MO0d',
      'password1': '',
      'phone_number': 123456789,
      'country': 'Somewhere',
      'city': 'Over',
      'street_address': 'The rainbow',
    }

    response = self.client.post('/api/users/', data)
    self.assertEqual(response.status_code, 400)

  def test_registration_invalid_email(self):
    data = {
      'email': 'madskills@mad',
      'username': 'IamAthlete2021',
      'password': '#¤554!"#MO0d',
      'password1': '#¤554!"#MO0d',
      'phone_number': 123456789,
      'country': 'Somewhere',
      'city': 'Over',
      'street_address': 'The rainbow',
    }

    response = self.client.post('/api/users/', data)
    self.assertEqual(response.status_code, 400)

  def test_registration_username_valid_special_characters(self):
    data = {
      'email': 'madskills@mad.mad',
      'username': '@.+-_IamAthlete2021',
      'password': '#¤554!"#MO0d',
      'password1': '#¤554!"#MO0d',
      'phone_number': 123456789,
      'country': 'Somewhere',
      'city': 'Over',
      'street_address': 'The rainbow',
    }

    response = self.client.post('/api/users/', data)
    self.assertEqual(response.status_code, 201)

  def test_registration_username_invalid_special_characters(self):
    data = {
      'email': 'madskills@mad.mad',
      'username': '[?!IamAthlete2021',
      'password': '#¤554!"#MO0d',
      'password1': '#¤554!"#MO0d',
      'phone_number': 123456789,
      'country': 'Somewhere',
      'city': 'Over',
      'street_address': 'The rainbow',
    }

    response = self.client.post('/api/users/', data)
    self.assertEqual(response.status_code, 400)


  def test_registration_not_valid_length_password(self):
    data = {
      'email': 'madskills@mad.mad',
      'username': 'IamAthlete2021',
      'password': 'a',
      'password1': 'a',
      'phone_number': 123456789,
      'country': 'Somewhere',
      'city': 'Over',
      'street_address': 'The rainbow',
    }

    response = self.client.post('/api/users/', data)
    self.assertEqual(response.status_code, 201)

  def test_registration_long_username(self):
    data = {
       'email': 'madskills@mad.mad',
      'username': 'IamAthlete2021IamAthlete2021IamAthlete2021IamAthlete2021IamAthlete2021IamAthlete2021IamAthlete2021IamAthlete2021IamAthlete2021IamAthlete2021IamAthlete2021',
      'password': '#¤554!"#MO0d',
      'password1': '#¤554!"#MO0d',
      'phone_number': 123456789,
      'country': 'Somewhere',
      'city': 'Over',
      'street_address': 'The rainbow',
    }

    response = self.client.post('/api/users/', data)
    self.assertEqual(response.status_code, 400)
    
  def test_registration_short_username(self):
    data = {
       'email': 'madskills@mad.mad',
      'username': 'i',
      'password': '#¤554!"#MO0d',
      'password1': '#¤554!"#MO0d',
      'phone_number': 123456789,
      'country': 'Somewhere',
      'city': 'Over',
      'street_address': 'The rainbow',
    }
    data['email'] = 'madskills@mad.mad'


    response = self.client.post('/api/users/', data)
    self.assertEqual(response.status_code, 201)

## BOUNDARY TESTS FOR NEW WORKOUT FORM ##
class NewWorkoutBoundary(TestCase):
    def setUp(self):
        self.client = Client()
        self.owner = User.objects.create(username="user")
        self.owner.set_password("password")
        self.owner.save()
        response = self.client.post("/api/token/", {"username": "user", "password": "password"})
        self.client.defaults["HTTP_AUTHORIZATION"] = "Bearer " + json.loads(response.content)["access"]

        self.data = {
            "name": "Test Workout",
            "date": "2021-03-08T18:56",
            "notes": "Test",
            "owner": self.owner,
            "visibility": "PU",
            "exercise_instances": "[]",
        }

    def test_workout_allgood(self):
        response = self.client.post("/api/workouts/", self.data)
        self.assertEqual(response.status_code, 201)

    def test_name_workout(self):
        # No name --> fail
        self.data["name"] = ""
        response = self.client.post("/api/workouts/", self.data)
        self.assertEqual(response.status_code, 400)

        # Min length name --> pass
        self.data["name"] = "W"
        response = self.client.post("/api/workouts/", self.data)
        self.assertEqual(response.status_code, 201)

        # Max length name (100 chars) --> pass
        self.data["name"] = "632IPHVZTBIW0H2EVSXLLPPFUCMGLPAXY26KXGSI0G571TE6Q46RN0GJ38XMZFL18L7QC3CN20ESZMEUEF8MSMSAJXX5TTMNZYJD"
        response = self.client.post("/api/workouts/", self.data)
        self.assertEqual(response.status_code, 201)

        # Max length + 1 char --> fail
        self.data["name"] = "632IPHVZTBIW0H2EVSXLLPPFUCMGLPAXY26KXGSI0G571TE6Q46RN0GJ38XMZFL18L7QC3CN20ESZMEUEF8MSMSAJXX5TTMNZYJDA"
        response = self.client.post("/api/workouts/", self.data)
        self.assertEqual(response.status_code, 400)

    def test_date_workout(self):
        # Empty date --> fail
        self.data["date"] = ""
        response = self.client.post("/api/workouts/", self.data)
        self.assertEqual(response.status_code, 400)

        # Min date --> pass, no validation
        self.data["date"] = "0001-01-01T00:00"
        response = self.client.post("/api/workouts/", self.data)
        self.assertEqual(response.status_code, 201)

        # Max date --> pass, no validation
        self.data["date"] = "9999-12-31T23:59"
        response = self.client.post("/api/workouts/", self.data)
        self.assertEqual(response.status_code, 201)

        # Max date + 1 --> fail 
        self.data["date"] = "10000-01-01T00:00"
        response = self.client.post("/api/workouts/", self.data)
        self.assertEqual(response.status_code, 400)

    def test_workout_notes(self):
        # Empty notes --> fail
        self.data["notes"] = ""
        response = self.client.post("/api/workouts/", self.data)
        self.assertEqual(response.status_code, 400)

        # Min length note --> pass
        self.data["notes"] = "a"
        response = self.client.post("/api/workouts/", self.data)
        self.assertEqual(response.status_code, 201)

# 2-WAY TESTS #

test_cases = [
  {'email':'normal',	'username':'normal', 'password':'empty', 'password1':'normal', 'phone_number':'empty', 'country':'normal',	'city':'empty',	'street_address':'normal'},
  {'email':'normal',	'username':'normal', 'password':'normal', 'password1':'empty', 'phone_number':'normal', 'country':'empty',	'city':'normal',	'street_address':'empty'},
  {'email':'normal',	'username':'empty', 'password':'normal', 'password1':'normal', 'phone_number':'empty', 'country':'normal',	'city':'empty',	'street_address':'normal'},
  {'email':'normal',	'username':'wrong', 'password':'empty', 'password1':'normal', 'phone_number':'normal', 'country':'empty',	'city':'normal',	'street_address':'empty'},
  {'email':'normal',	'username':'normal', 'password':'normal', 'password1':'empty', 'phone_number':'normal', 'country':'normal',	'city':'empty',	'street_address':'normal'},
  {'email':'normal',	'username':'empty', 'password':'empty', 'password1':'normal', 'phone_number':'empty', 'country':'normal',	'city':'normal',	'street_address':'empty'},
  {'email':'normal',	'username':'wrong', 'password':'normal', 'password1':'empty', 'phone_number':'normal', 'country':'empty',	'city':'normal',	'street_address':'normal'},    
  {'email':'wrong',	'username':'wrong', 'password':'normal', 'password1':'normal', 'phone_number':'empty', 'country':'empty',	'city':'empty',	'street_address':'normal'},
  {'email':'wrong',	'username':'normal', 'password':'empty', 'password1':'normal', 'phone_number':'normal', 'country':'normal',	'city':'normal',	'street_address':'empty'},
  {'email':'wrong',	'username':'empty', 'password':'normal', 'password1':'empty', 'phone_number':'empty', 'country':'empty',	'city':'normal',	'street_address':'normal'},
  {'email':'wrong',	'username':'normal', 'password':'empty', 'password1':'empty', 'phone_number':'empty', 'country':'normal',	'city':'normal',	'street_address':'normal'},
  {'email':'wrong',	'username':'normal', 'password':'normal', 'password1':'normal', 'phone_number':'normal', 'country':'empty',	'city':'empty',	'street_address':'empty'},
  {'email':'normal',	'username':'wrong', 'password':'empty', 'password1':'normal', 'phone_number':'normal', 'country':'empty',	'city':'normal',	'street_address':'normal'},
  {'email':'normal',	'username':'normal', 'password':'normal', 'password1':'empty', 'phone_number':'empty', 'country':'normal',	'city':'normal',	'street_address':'empty'},
  {'email':'normal',	'username':'empty', 'password':'normal', 'password1':'normal', 'phone_number':'empty', 'country':'normal',	'city':'normal',	'street_address':'empty'},
  {'email':'normal',	'username':'wrong', 'password':'empty', 'password1':'empty', 'phone_number':'normal', 'country':'normal',	'city':'empty',	'street_address':'normal'},
  {'email':'empty',	'username':'empty', 'password':'empty', 'password1':'empty', 'phone_number':'normal', 'country':'normal',	'city':'empty',	'street_address':'normal'},
  {'email':'empty',	'username':'wrong', 'password':'normal', 'password1':'normal', 'phone_number':'empty', 'country':'normal',	'city':'normal',	'street_address':'empty'},
  {'email':'empty',	'username':'normal', 'password':'empty', 'password1':'normal', 'phone_number':'normal', 'country':'empty',	'city':'empty',	'street_address':'normal'},
  {'email':'empty',	'username':'normal', 'password':'normal', 'password1':'empty', 'phone_number':'empty', 'country':'normal',	'city':'normal',	'street_address':'empty'},
  {'email':'empty',	'username':'empty', 'password':'empty', 'password1':'normal', 'phone_number':'normal', 'country':'empty',	'city':'normal',	'street_address':'normal'},
  {'email':'empty',	'username':'wrong', 'password':'normal', 'password1':'empty', 'phone_number':'normal', 'country':'normal',	'city':'empty',	'street_address':'empty'},
  {'email':'empty',	'username':'normal', 'password':'normal', 'password1':'normal', 'phone_number':'empty', 'country':'empty',	'city':'normal',	'street_address':'normal'},
  {'email':'wrong',	'username':'wrong', 'password':'empty', 'password1':'empty', 'phone_number':'normal', 'country':'normal',	'city':'normal',	'street_address':'normal'},
  {'email':'wrong',	'username':'empty', 'password':'normal', 'password1':'normal', 'phone_number':'normal', 'country':'normal',	'city':'empty',	'street_address':'empty'},
  {'email':'wrong',	'username':'wrong', 'password':'normal', 'password1':'empty', 'phone_number':'empty', 'country':'empty',	'city':'normal',	'street_address':'normal'},
  {'email':'wrong',	'username':'empty', 'password':'normal', 'password1':'normal', 'phone_number':'empty', 'country':'empty',	'city':'empty',	'street_address':'normal'}

]

case_data = {
  'normal': {
    'email': 'madskills@mad.mad',
      'username': 'IamAthlete2021',
      'password': '#¤554!"#MO0d',
      'password1': '#¤554!"#MO0d',
      'phone_number': 123456789,
      'country': 'Somewhere',
      'city': 'Over',
      'street_address': 'The rainbow',
  },
  'empty': {
    'email': '',
    'username': '',
    'password': '',
    'password1': '',
    'phone_number': '',
    'country': '',
    'city': '',
    'street_address': '',
  },
  'wrong': {
    'email': 'madskills@mad',
    'username': '[IamAthlete2021',
  }
}

class RegistrationTwoWayTest(TestCase):
  def setUp(self):
    self.client = Client()

  def test_registration_two_way(self):
    
    for case in test_cases:
      data = {}
      for key, value in case.items():
        data[key] = case_data[value][key]
      code = 201
      if  case['password'] == 'empty' or case['username'] == 'empty' or case['password1'] == 'empty' or case['email'] == 'wrong' or case['username'] == 'wrong':
        code = 400
      response = self.client.post('/api/users/', data)
      self.assertEqual(response.status_code, code)

class VisibilityWorkoutTest(TestCase):
  def setUp(self):
    self.client = Client()
    self.coach = User.objects.create(username="coach")
    self.coach.set_password('12345')
    self.coach.save()
    self.athlete = User.objects.create(username="athlete", coach=self.coach)
    self.athlete.set_password('12345')
    self.athlete.save()
    self.other = User.objects.create(username="other")

  def test_athlete_can_view_workouts(self):
    response = self.client.post('/api/token/', {'username': 'athlete', 'password': '12345'})
    content = json.loads(response.content)
    self.client.defaults['HTTP_AUTHORIZATION'] = 'Bearer ' + content['access']

    # Athlete can see own private workout
    workout = Workout.objects.create(name="Workout", date="2021-03-05T12:00:00Z", notes="Notes", owner=self.athlete, visibility="PR")
    response = self.client.get('/api/workouts/'+str(workout.id)+'/')
    self.assertEqual(response.status_code, 200)
    comment = Comment.objects.create(content="COMMENT", timestamp="2021-03-05T12:00:00Z", owner=self.athlete, workout=workout)
    response = self.client.get('/api/comments/'+str(comment.id)+'/')
    self.assertEqual(response.status_code, 200)
    file = WorkoutFile.objects.create(file='some/path/to/file', owner=self.athlete, workout=workout)
    response = self.client.get('/api/workout-files/'+str(file.id)+'/')
    self.assertEqual(response.status_code, 200)

    # Athlete can see public workout
    workout = Workout.objects.create(name="Workout", date="2021-03-05T12:00:00Z", notes="Notes", owner=self.other, visibility="PU")
    response = self.client.get('/api/workouts/'+str(workout.id)+'/')
    self.assertEqual(response.status_code, 200)
    comment = Comment.objects.create(content="COMMENT", timestamp="2021-03-05T12:00:00Z", owner=self.other, workout=workout)
    response = self.client.get('/api/comments/'+str(comment.id)+'/')
    self.assertEqual(response.status_code, 200)
    file = WorkoutFile.objects.create(file='some/path/to/file', owner=self.other, workout=workout)
    response = self.client.get('/api/workout-files/'+str(file.id)+'/')
    self.assertEqual(response.status_code, 200)

    # Athlete cannot see private workout
    workout = Workout.objects.create(name="Workout", date="2021-03-05T12:00:00Z", notes="Notes", owner=self.other, visibility="PR")
    response = self.client.get('/api/workouts/'+str(workout.id)+'/')
    self.assertEqual(response.status_code, 403)
    comment = Comment.objects.create(content="COMMENT", timestamp="2021-03-05T12:00:00Z", owner=self.other, workout=workout)
    response = self.client.get('/api/comments/'+str(comment.id)+'/')
    self.assertEqual(response.status_code, 403)
    file = WorkoutFile.objects.create(file='some/path/to/file', owner=self.other, workout=workout)
    response = self.client.get('/api/workout-files/'+str(file.id)+'/')
    self.assertEqual(response.status_code, 403)

    # Athlete cannot see coach-visibility workout
    workout = Workout.objects.create(name="Workout", date="2021-03-05T12:00:00Z", notes="Notes", owner=self.other, visibility="CO")
    response = self.client.get('/api/workouts/'+str(workout.id)+'/')
    self.assertEqual(response.status_code, 403)
    comment = Comment.objects.create(content="COMMENT", timestamp="2021-03-05T12:00:00Z", owner=self.other, workout=workout)
    response = self.client.get('/api/comments/'+str(comment.id)+'/')
    self.assertEqual(response.status_code, 403)
    file = WorkoutFile.objects.create(file='some/path/to/file', owner=self.other, workout=workout)
    response = self.client.get('/api/workout-files/'+str(file.id)+'/')
    self.assertEqual(response.status_code, 403)

  def test_coach_sees_workouts(self):
    response = self.client.post('/api/token/', {'username': 'coach', 'password': '12345'})
    content = json.loads(response.content)
    self.client.defaults['HTTP_AUTHORIZATION'] = 'Bearer ' + content['access']

    # Coach can see public athlete workout
    workout = Workout.objects.create(name="Workout", date="2021-03-05T12:00:00Z", notes="Notes", owner=self.athlete, visibility="PU")
    response = self.client.get('/api/workouts/'+str(workout.id)+'/')
    self.assertEqual(response.status_code, 200)
    comment = Comment.objects.create(content="COMMENT", timestamp="2021-03-05T12:00:00Z", owner=self.athlete, workout=workout)
    response = self.client.get('/api/comments/'+str(comment.id)+'/')
    self.assertEqual(response.status_code, 200)
    file = WorkoutFile.objects.create(file='some/path/to/file', owner=self.athlete, workout=workout)
    response = self.client.get('/api/workout-files/'+str(file.id)+'/')
    self.assertEqual(response.status_code, 200)

    # Coach can see coach-visibility athlete workout
    workout = Workout.objects.create(name="Workout", date="2021-03-05T12:00:00Z", notes="Notes", owner=self.athlete, visibility="CO")
    response = self.client.get('/api/workouts/'+str(workout.id)+'/')
    self.assertEqual(response.status_code, 200)
    comment = Comment.objects.create(content="COMMENT", timestamp="2021-03-05T12:00:00Z", owner=self.coach, workout=workout)
    response = self.client.get('/api/comments/'+str(comment.id)+'/')
    self.assertEqual(response.status_code, 200)
    file = WorkoutFile.objects.create(file='some/path/to/file', owner=self.coach, workout=workout)
    response = self.client.get('/api/workout-files/'+str(file.id)+'/')
    self.assertEqual(response.status_code, 200)

    # Coach can see public other workout
    workout = Workout.objects.create(name="Workout", date="2021-03-05T12:00:00Z", notes="Notes", owner=self.other, visibility="PU")
    response = self.client.get('/api/workouts/'+str(workout.id)+'/')
    self.assertEqual(response.status_code, 200)
    comment = Comment.objects.create(content="COMMENT", timestamp="2021-03-05T12:00:00Z", owner=self.coach, workout=workout)
    response = self.client.get('/api/comments/'+str(comment.id)+'/')
    self.assertEqual(response.status_code, 200)
    file = WorkoutFile.objects.create(file='some/path/to/file', owner=self.coach, workout=workout)
    response = self.client.get('/api/workout-files/'+str(file.id)+'/')
    self.assertEqual(response.status_code, 200)

    #Coach cannot see private athlete workout (DENNE FEILER)
    #workout = Workout.objects.create(name="Workout", date="2021-03-05T12:00:00Z", notes="Notes", owner=self.athlete, visibility="PR")
    #response = self.client.get('/api/workouts/'+str(workout.id)+'/')
    #self.assertEqual(response.status_code, 403)
    #comment = Comment.objects.create(content="COMMENT", timestamp="2021-03-05T12:00:00Z", owner=self.other, workout=workout)
    #response = self.client.get('/api/comments/'+str(comment.id)+'/')
    #self.assertEqual(response.status_code, 403)
    #file = WorkoutFile.objects.create(file='some/path/to/file', owner=self.athlete, workout=workout)
    #response = self.client.get('/api/workout-files/'+str(file.id)+'/')
    #self.assertEqual(response.status_code, 403)

    # Coach cannot see private other workout
    workout = Workout.objects.create(name="Workout", date="2021-03-05T12:00:00Z", notes="Notes", owner=self.other, visibility="PR")
    response = self.client.get('/api/workouts/'+str(workout.id)+'/')
    self.assertEqual(response.status_code, 403)
    comment = Comment.objects.create(content="COMMENT", timestamp="2021-03-05T12:00:00Z", owner=self.other, workout=workout)
    response = self.client.get('/api/comments/'+str(comment.id)+'/')
    self.assertEqual(response.status_code, 403)
    file = WorkoutFile.objects.create(file='some/path/to/file', owner=self.other, workout=workout)
    response = self.client.get('/api/workout-files/'+str(file.id)+'/')
    self.assertEqual(response.status_code, 403)
    

    
    # Coach cannot see coach-visibility other workout
    workout = Workout.objects.create(name="Workout", date="2021-03-05T12:00:00Z", notes="Notes", owner=self.other, visibility="CO")
    response = self.client.get('/api/workouts/'+str(workout.id)+'/')
    self.assertEqual(response.status_code, 403)
    comment = Comment.objects.create(content="COMMENT", timestamp="2021-03-05T12:00:00Z", owner=self.other, workout=workout)
    response = self.client.get('/api/comments/'+str(comment.id)+'/')
    self.assertEqual(response.status_code, 403)
    file = WorkoutFile.objects.create(file='some/path/to/file', owner=self.other, workout=workout)
    response = self.client.get('/api/workout-files/'+str(file.id)+'/')
    self.assertEqual(response.status_code, 403)


    
