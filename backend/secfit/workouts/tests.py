"""
Tests for the workouts application.
"""
from django.test import TestCase, RequestFactory
from django.contrib.sessions.middleware import SessionMiddleware
from users.models import User
from workouts.permissions import IsOwner, IsOwnerOfWorkout, IsCoachAndVisibleToCoach, IsCoachOfWorkoutAndVisibleToCoach, IsPublic, IsWorkoutPublic, IsReadOnly
from workouts.models import Workout, Exercise, ExerciseInstance

# Test for IsOwner permission
class IsOwnerTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        
        self.user = User.objects.create(username="testuser")
        self.user2 = User.objects.create(username="testuser2")
        
    def test_isowner(self):
        req = self.factory.get("/")
        req.user = self.user
        
        req2 = self.factory.get("/")
        req2.user = self.user2
        
        workout = Workout.objects.create(name="Test Workout", date="2021-03-14T12:12:00Z", notes="TEST", owner=self.user, visibility="PU")
        
        permission = IsOwner().has_object_permission(req, None, workout)
        permission2 = IsOwner().has_object_permission(req2, None, workout)
        
        self.assertTrue(permission)
        self.assertFalse(permission2)

# Test for IsOwnerOfWorkout permission
class IsOwnerOfWorkoutTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        
        self.user = User.objects.create(username="testuser")
        self.user2 = User.objects.create(username="testuser2")
        
    def test_isOwnerOfWorkout(self):

        req = self.factory.get("/")
        req.user = self.user
        
        req2 = self.factory.get("/")
        req2.user = self.user2
        
        workout = Workout.objects.create(name="Test Workout", date="2021-03-14T12:12:00Z", notes="TEST", owner=self.user, visibility="PU")
        exercise = Exercise.objects.create(name="Test Exercise", description="TEST", unit="Amount")
        instance = ExerciseInstance.objects.create(workout=workout, exercise=exercise, sets=5, number=5)
        
        permission = IsOwnerOfWorkout().has_object_permission(req, None, instance)
        permission2 = IsOwnerOfWorkout().has_object_permission(req2, None, instance)
        
        self.assertTrue(permission)
        self.assertFalse(permission2)

# Test for IsCoachAndVisibleToCoach permission
class IsCoachAndVisibleToCoachTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.middleware = SessionMiddleware()
            
        self.coachuser = User.objects.create(username="coachuser")
        self.user = User.objects.create(username="testuser", coach=self.coachuser)
        
    def test_isCoachAndVisibleToCoach(self):
        req = self.factory.get("/")
        req.user = self.user
        self.middleware.process_request(req)
        req.session.save()
        
        req2 = self.factory.get("/")
        req2.user = self.coachuser
        self.middleware.process_request(req2)
        req2.session.save()
        
        
        workout = Workout.objects.create(name="Test Workout", date="2021-03-14T12:12:00Z", notes="TEST", owner=self.user, visibility="CO")
        
        permission = IsCoachAndVisibleToCoach().has_object_permission(req, None, workout)
        permission2 = IsCoachAndVisibleToCoach().has_object_permission(req2, None, workout)
        
        self.assertFalse(permission)
        self.assertTrue(permission2)

# Test for IsCoachOfWorkoutAndVisibleToCoach permission
class IsCoachOfWorkoutAndVisibleToCoachTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.middleware = SessionMiddleware()
                
        self.coachuser = User.objects.create(username="coachuser")
        self.user = User.objects.create(username="testuser", coach=self.coachuser)
          
    def test_IsCoachOfWorkoutAndVisibleToCoach(self):
        req = self.factory.get("/")      
        req.user = self.user
        self.middleware.process_request(req)    
        req.session.save()
        
        req2 = self.factory.get("/")
        req2.user = self.coachuser
        self.middleware.process_request(req2)
        req2.session.save()
        
        workout = Workout.objects.create(name="Test Workout", date="2021-03-14T12:12:00Z", notes="TEST", owner=self.user, visibility="PU")    
        exercise = Exercise.objects.create(name="Test Exercise Public", description="TEST", unit="Amount")     
        instance = ExerciseInstance.objects.create(workout=workout, exercise=exercise, sets=10, number=10)
        
        permission = IsCoachOfWorkoutAndVisibleToCoach().has_object_permission(req, None, instance)
        permission2 = IsCoachOfWorkoutAndVisibleToCoach().has_object_permission(req2, None, instance)
        
        self.assertFalse(permission)
        self.assertTrue(permission2)

# Test for IsPublic permission
class IsPublicTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        
        self.user = User.objects.create(username="testuser")
        
    def test_isPublic(self):
        req = self.factory.get("/")
        req.user = self.user
        
        workout_public = Workout.objects.create(name="Test Workout Public", date="2021-03-14T12:12:00Z", notes="TEST", owner=self.user, visibility="PU")
        workout_private = Workout.objects.create(name="Test Workout Private", date="2021-03-14T12:12:00Z", notes="TEST", owner=self.user, visibility="PR")
        
        permission = IsPublic().has_object_permission(req, None, workout_public)
        permission2 = IsPublic().has_object_permission(req, None, workout_private)
        
        self.assertTrue(permission)
        self.assertFalse(permission2)

# Test for IsWorkoutPublic permission
class IsWorkoutPublicTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        
        self.user = User.objects.create(username="testuser")
        
    def test_isWorkoutPublic(self):
        req = self.factory.get("/")
        req.user = self.user
        
        workout_public = Workout.objects.create(name="Test Workout Public", date="2021-03-14T12:12:00Z", notes="TEST", owner=self.user, visibility="PU")
        workout_private = Workout.objects.create(name="Test Workout Private", date="2021-03-14T12:12:00Z", notes="TEST", owner=self.user, visibility="PR")
        exercise = Exercise.objects.create(name="Test Exercise Public", description="TEST", unit="Amount")
        
        instance_public = ExerciseInstance.objects.create(workout=workout_public, exercise=exercise, sets=10, number=10)
        instance_private = ExerciseInstance.objects.create(workout=workout_private, exercise=exercise, sets=10, number=10)
        
        permission = IsWorkoutPublic().has_object_permission(req, None, instance_public)
        permission2 = IsWorkoutPublic().has_object_permission(req, None, instance_private)
        
        self.assertTrue(permission)
        self.assertFalse(permission2)

# Test for IsReadOnly permission
class IsReadOnlyTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        
        self.user = User.objects.create(username="testuser")
        self.user2 = User.objects.create(username="testuser2")
        self.user3 = User.objects.create(username="testuser3")

    def test_IsReadOnly(self):
        req = self.factory.get("/")
        req.user = self.user
        
        req2 = self.factory.get("/")
        req.user = self.user2
        
        req3 = self.factory.get("/")
        req.user = self.user3
        
        workout = Workout.objects.create(name="Test Workout", date="2021-03-14T12:12:00Z", notes="TEST", owner=self.user, visibility="PU")
        
        permission = IsReadOnly().has_object_permission(req, None, workout)
        permission2 = IsReadOnly().has_object_permission(req2, None, workout)
        permission3 = IsReadOnly().has_object_permission(req3, None, workout)
        
        self.assertTrue(permission)
        self.assertTrue(permission2)
        self.assertTrue(permission3)

