"""
Module for registering models from workouts app to admin page so that they appear
"""
from django.contrib import admin

# Register your models here.
from .models import Exercise, ExerciseInstance, Workout, WorkoutFile

registerList = [Exercise, ExerciseInstance, Workout, WorkoutFile]
for item in registerList: 
    admin.site.register(item)